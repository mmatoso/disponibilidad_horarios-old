"""
Define los cursores a utilizar para conectarse a las bases de datos
"""
import pymysql.cursors

from ip_variables import *

consulta_disponibilidad = pymysql.connect(
    host=HOST_CHURRINCHE,
    user=USER_DISPONIBILIDAD,
    password=PASS_DISPONIBILIDAD,
    db=DB_NAME_DISPONIBILIDAD,
    cursorclass=pymysql.cursors.DictCursor
)

consulta_zabbix = pymysql.connect(
    host=HOST_ZABBIX,
    user=USER_ZABBIX,
    password=PASS_ZABBIX,
    db=DB_NAME_ZABBIX,
    cursorclass=pymysql.cursors.DictCursor
)