"""
El modulo tiene la intención de obtener datos referidos a la base de datos de disponibilidad.
Los datos obtenidos aquí provienen de la base disponibilidad_horarios y de sus tablas relacionadas.
Dicha base se encuentra alojada en el servidor de churrinche.
"""

import numpy as np
import pymysql.cursors
from datetime import datetime, timedelta

from CRM.webServiceCrm import datosGeneralesCRM, datosRegistradosCRM
from datos_bdd_and_crm.cursores_bdd import consulta_disponibilidad
from zabbix.datos_zabbix import obtener_locales_grupos_infra, obtener_disponibilidad, es_produccion
from ip_variables import *


def obtener_mediana_alumnos_ultimo_anio(codicen):
    """
    Obtiene los ultimos 5 meses del anio anterior a partir de la fecha actual
    :param codicen: el codicen para buscar los alumnos
    :return: mediana de alumnos ultimos 5 meses año anterior
    """
    anio_actual = datetime.now().strftime('%Y')
    anio_anterior = int(anio_actual) - 1
    mes_ini = "{}-07-01".format(anio_anterior)
    mes_fin = "{}-12-01".format(anio_anterior)

    consulta = f"""SELECT alumnos FROM `locales` WHERE 
     codicen = '{codicen}' AND mes > '{mes_ini}' AND mes <= '{mes_fin}'"""

    with consulta_disponibilidad.cursor() as cursor:
        cursor.execute(consulta)
        alumnos = cursor.fetchall()

    mediana_alumnos = None

    if alumnos:
        lista_alumnos = [alumno.get('alumnos') for alumno in alumnos]
        lista_alumnos = [alumno for alumno in lista_alumnos if alumno is not None]  # Elimino Nones values

        if lista_alumnos:
            mediana_alumnos = int(round(np.median(lista_alumnos)))

    return mediana_alumnos


def obtener_lista_locales_tabla_locales(mes_string):
    """
    Obtiene para un mes determinado una lista de locales de la tabla de locales
    :param mes_string: una fecha como string '2020-10-01'
    :return: lista con codicenes correspondientes al mes seleccionado
    """

    # Consulta para obtener los codicen de la bdd disponibilidad para determinado mes.
    query_locales_disponibilidad = f"SELECT distinct codicen FROM `locales` WHERE mes = '{mes_string}'"

    with consulta_disponibilidad.cursor() as cursor:
        cursor.execute(query_locales_disponibilidad)
        lista_locales_tabla_disponibilidad = cursor.fetchall()

    lista_locales_tabla_disponibilidad = [item.get('codicen') for item in lista_locales_tabla_disponibilidad]

    return lista_locales_tabla_disponibilidad


def insertar_datos_tabla_locales(tupla_insertar):
    """
    Se encarga de insertar en la tabla de locales la tupla que recibe por parametro.
    :param tupla_insertar: la tupla con los datos a insertar en base
    :return:
    """

    conn = pymysql.connect(
        host=HOST_CHURRINCHE,
        user=USER_DISPONIBILIDAD,
        password=PASS_DISPONIBILIDAD,
        db=DB_NAME_DISPONIBILIDAD,
        cursorclass=pymysql.cursors.DictCursor
    )

    # Consulta para insertar en base de datos en caso de diferencia en la lista diaria.
    query_dif = """INSERT INTO locales (codicen, mes, acceso, crm, educativo, tipo_local,
                alumnos, ad, altaPerformance, depto)
                VALUES(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"""

    with conn.cursor() as cursor:
        cursor.execute(query_dif, tupla_insertar)

    conn.commit()
    conn.close()


def existe_mes(mes_string):
    """
    Verifica si ya existe el mes seleccionado en la base de datos y la tabla locales.
    En base a si existe o no el mes
    :param mes_string: un string que representa el mes '2020-10-01'
    :return: True or False
    """

    # Verifico si existe el mes corriente insertado en la base de datos.
    query_existe_el_mes = f"SELECT mes FROM disponibilidad_horarios.locales WHERE mes = '{mes_string}' LIMIT 1"

    with consulta_disponibilidad.cursor() as cursor:
        cursor.execute(query_existe_el_mes)
        existe_mes = cursor.fetchall()

    existe_mes = True if existe_mes else False

    return existe_mes


def lista_de_locales(mes_string):

    """
    Obtiene una lista de los locales en produccion, ademas si encuentra diferencias diarias por locales que se dan
    de alta lo detecta y los agrega a la base y actualiza la lista.
    Corre diariamente para buscar las diferencias.
    Si es primero de mes saca la lista completa de hosts de zabbix,
    si no utiliza la lista de la base de datos disponibilidad.
    :param mes_string: el mes como string '2020-10-01'
    :return: lista de locales
    """
    locales_infra_zabbix = obtener_locales_grupos_infra()  # locales infra de zabbix
    locales_infra_zabbix = [codicen.strip(' ') for codicen in locales_infra_zabbix]  # Elimino espacios en codicen

    mes = existe_mes(mes_string)  # existe mes en tabla locales de disponibilidad

    if mes:  # si existe el mes en la base de datos, se buscan diferencias

        lista_locales_tabla_disponibilidad = obtener_lista_locales_tabla_locales(mes_string)
        lista_locales_tabla_disponibilidad = [codicen.strip(' ') for codicen in lista_locales_tabla_disponibilidad]

        # Comparo ambas listas para buscar diferencias.
        diferencias = set(locales_infra_zabbix) - set(lista_locales_tabla_disponibilidad)

        if diferencias:
            for codicen in diferencias:
                # si hay diferencias paso un Kwarg
                tupla_local_nuevo = generar_tupla_tabla_locales(codicen, mes_string)
                insertar_datos_tabla_locales(tupla_local_nuevo)

        # Por ultimo busco la nueva lista si es que hay codicen nuevos en la base de datos de disponibilidad.
        locales_base_disponibilidad = obtener_lista_locales_tabla_locales(mes_string)

        locales_base_disponibilidad = set([item for item in locales_base_disponibilidad])
        locales_base_disponibilidad = list(locales_base_disponibilidad)

        return locales_base_disponibilidad

    else:
        # Si no existe el mes retorno la lista completa de zabbix
        return locales_infra_zabbix


def generar_tupla_tabla_locales(codicen, mes_string):
    """
    Funcion que genera una tupla para guardar datos en la tabla de locales, base de datos disponibilidad_horarios
    :param codicen: el codicen el cual buscar datos
    :param mes_string: primero de mes como string '2020-10-01'
    :return: tupla con todos los datos
    """

    locales_educativos = [
        'Escuela Publica',
        'Utu',
        'Liceo público',
        'Centro de Formacion Docente Publico',
        'Centro de lenguas Extranjeras',
        'Escuela Publica Especial',
        'Inspeccion',
        'Jardin publico',
        'Liceos Privados Gratuitos',
        'Satelite o Anexo'
    ]

    try:
        datos_generales = datosGeneralesCRM(codicen)
    except KeyError:
        acceso = None
        crm = False
        educativo = False
        tipo_local = None
        alumnos = None
        ad = None
        alta_performance = None
        depto = None
    else:

        crm = True
        cnumber = datos_generales['cnumber']
        datos_registrados = datosRegistradosCRM(cnumber, codicen)

        try:
            acceso = datos_registrados['Acceso'].encode('utf-8')
        except:
            acceso = None

        try:
            tipo_local = datos_generales['tipolocal']
        except:
            tipo_local = None

        # Educativo?
        if tipo_local in locales_educativos:
            educativo = True
        else:
            educativo = False

        # Si el local se agrego en el correr del a;o, no va a retornar un valor la funcion \
        # mediana_alumnos_ultimo_anio(), entonces se va a buscar el valor que tiene en CRM, \
        # unicamente si fallan ambos van a retornar None
        try:
            # alumnos = datosRegistrados['Cantidad de alumnos']
            alumnos = obtener_mediana_alumnos_ultimo_anio(codicen)
            if alumnos is None:
                try:
                    alumnos = datos_registrados['Cantidad de alumnos']
                except:
                    alumnos = None
        except:
            alumnos = None

        try:
            ad = datos_registrados['Alta Disponibilidad']
        except:
            ad = False
        else:
            if ad.upper() == 'SI':
                ad = True
            else:
                ad = False

        try:
            alta_performance = datos_registrados['Alta Performance']
        except:
            alta_performance = False
        else:
            if alta_performance.upper() == 'SI':
                alta_performance = True
            else:
                alta_performance = False

        try:
            depto = datos_generales['departamento']
        except:
            depto = None

    tupla_escuela = (
        codicen,
        mes_string,
        acceso,
        crm,
        educativo,
        tipo_local,
        alumnos,
        ad,
        alta_performance,
        depto
    )

    return tupla_escuela


def generar_tupla_tabla_disponibilidad(codicen, mes_string, dia_string):

    """Crea una tupla para insertar en la tabla de disponibilidad"""

    # Si el local no se encuentra en CRM y no tiene datos se considera como no produccion
    try:
        datosGeneralesCRM(codicen)
    except KeyError:
        disponibilidad = obtener_disponibilidad(codicen, dia_string)
        prod = False
    else:
        prod = es_produccion(codicen)
        disponibilidad = obtener_disponibilidad(codicen, dia_string)

    tupla_disponibilidad = (dia_string, disponibilidad, codicen, mes_string, prod)

    return tupla_disponibilidad
