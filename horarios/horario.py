import pymysql.cursors
from CRM.webServiceCrm import datosGeneralesCRM, datosRegistradosCRM
from ip_variables import *


class Horarios:

	"""Obtiene la franja horaria que corresponde a un local educativo"""

	ces = ['Liceo público', 'Centro de Lenguas Extranjeras', 'Liceos Privados Gratuitos']
	cetp = ['Utu']
	ceip = ['Escuela Publica', 'Escuela Publica Especial', 'Jardin Publico']
	cfe = ['Centro de Formacion Docente Publico', 'Inspeccion']

	consulta_disponibilidad = pymysql.connect(
		host=HOST_CHURRINCHE,
		user=USER_DISPONIBILIDAD,
		password=PASS_DISPONIBILIDAD,
		db=DB_NAME_DISPONIBILIDAD,
		cursorclass=pymysql.cursors.DictCursor
	)

	def __init__(self, codicen):

		self.codicen = codicen
		try:
			self.datos_generales_crm = datosGeneralesCRM(self.codicen)
			self.cnumber = self.datos_generales_crm['cnumber']
			self.datos_registrados_crm = datosRegistradosCRM(self.cnumber, self.codicen)
		except KeyError:
			raise ValueError('El codicen ingresado: {} no existe'.format(self.codicen))

	def obtengo_tipo_local(self):
		"""Obtiene de crm el tipo de local"""
		tipo_local = self.datos_generales_crm.get('tipolocal', None)

		return tipo_local

	def obtengo_rubro(self):
		"""Obtiene el rubro del local a partir del tipo de local"""
		tipo_local = self.obtengo_tipo_local()

		if tipo_local in self.ces:
			return 'ces'
		elif tipo_local in self.cetp:
			return 'cetp'
		elif tipo_local in self.ceip:
			return 'ceip'
		elif tipo_local in self.cfe:
			return 'cfe'
		else:
			return None

	def obtengo_cobertura_turno(self):
		"""Obtiene el atributo cobertura turno de CRM"""

		cobertura_turno = self.datos_registrados_crm.get('Cobertura turnos', None)

		return cobertura_turno

	def _filtro_turno_concatenado(self):
		"""Elimina espacios en el principio y final de cada string obtenido de turno cobertura
		return: retorna una lista con los turnos concatenados, puede ser uno o mas de uno
		"""

		cobertura_turno = self.obtengo_cobertura_turno()

		if cobertura_turno:

			turnos = cobertura_turno.split('/')
			lista_turnos = list()

			for turno in turnos:
				lista_turnos.append(turno.strip(' ').lower())

			return lista_turnos

		else:
			return None

	def _turnos_borde(self):
		"""
		A partir de la lista de turnos concatenados se obtienen los turnos de los extremos
		esto ayuda a tener el rango mas amplio que puede tener un local educativo.
		"""
		lista_turnos_concatenados = self._filtro_turno_concatenado()

		if lista_turnos_concatenados:
			largo = len(lista_turnos_concatenados)
			lista_extremos = list()

			# Se asume que puede tener uno o mas valores, nunca 0
			if largo == 1:
				return lista_turnos_concatenados
			elif largo > 1:
				lista_extremos.append(lista_turnos_concatenados[0])
				lista_extremos.append(lista_turnos_concatenados[-1])
				return lista_extremos
		else:
			return None

	def _corrijo_turnos_abreviados(self, turno_abreviado):
		"""Parsea un turno abreviado, ideal para usar con función map()"""

		diccionario_parseo = {
			'mat': 'matutino',
			'e': 'extendido',
			'int': 'intermedio',
			'noc': 'nocturno',
			'ves': 'vespertino',
		}

		string_turno = ''

		for turno in turno_abreviado.split(' '):
			string_turno += '{} '.format(diccionario_parseo.get(turno, turno))

		string_turno = string_turno.strip(' ')

		return string_turno

	def obtengo_turnos(self):
		"""Formatealos los turnos apropiadamente para que sean legibles"""

		turnos = self._turnos_borde()
		turno_por_defecto = ['defecto']  # Matchea con la key del diccionario con horario por defecto

		if turnos:
			turnos_formateados = list(map(self._corrijo_turnos_abreviados, turnos))
			return turnos_formateados

		else:
			return turno_por_defecto

	def obtengo_diccionario_horarios(self, rubro):
		"""Devuelve el diccionario correspondiente basado en el rubro del local"""

		if rubro == 'ces':
			string_rubro = 'ces'

		elif rubro == 'cetp':
			string_rubro = 'cetp'

		elif rubro == 'ceip':
			string_rubro = 'ceip'

		# elif rubro == 'cfe':
		# 	# Debe de tener su propio mapeo, no solo el por defecto
		# 	diccionario_horarios = mapeo_horario_por_defecto

		else:
			string_rubro = 'ceip'  # Todos los diccionarios tienen el horario por defecto, se obtiene uno cualquiera

		with self.consulta_disponibilidad.cursor() as cursor:
			cursor.execute('select * from horarios_{}'.format(string_rubro))
			horarios = cursor.fetchall()

		return horarios

	@staticmethod
	def _obtengo_horario_de_rubro(diccionario_horario_rubro, turno_a_buscar):
		"""
		A partir del diccionario de un rubro y el turno, se retorna el horario para el mismo
		:param diccionario_horario_rubro: el diccionario que contiene todos los horarios y turnos
		:param turno_a_buscar: el turno a buscar dentro del diccionario
		:return: tupla con la franja horaria: ('- 7200', '+ 50400')
		"""

		diccionario_horarios = dict()

		for horario in diccionario_horario_rubro:

			turno = horario.get('turno')
			if turno == turno_a_buscar:
				diccionario_horarios = horario
				break

		horario_inicial = (
			diccionario_horarios.get('horario_inicio_desde_10am'),
			diccionario_horarios.get('horario_inicio')
		)
		horario_final = (
			diccionario_horarios.get('horario_final_desde_10am'),
			diccionario_horarios.get('horario_final')

		)
		franja_horaria = (horario_inicial, horario_final)

		return franja_horaria

	def obtengo_horarios(self):
		"""Obtengo los horarios para el local educativo"""

		rubro_local = self.obtengo_rubro()  # Puede ser None
		turnos_local = self.obtengo_turnos()  # Puede no tener el atributo Cobertura Turno y devolver 'defecto'
		largo_de_lista_turnos = len(turnos_local)
		rubros_de_interes = ['ces', 'cetp', 'ceip']

		if turnos_local[0] != 'defecto' and rubro_local in rubros_de_interes:

			diccionario_horarios = self.obtengo_diccionario_horarios(rubro_local)

			# El primer turno siempre se refiere al rubro, el segundo turno puede variar
			# dependiendo si comparte local con alguna institucion de otro rubro
			# se necesita chequear si en el segundo turno existe un horario de otro rubro que no corresponde con el primero

			# Si tiene este largo la lista puede que el segundo horario corresponda con otro rubro
			# diferente al que pertenece el primer horario
			if largo_de_lista_turnos == 2:
				primer_turno = turnos_local[0]
				segundo_turno = turnos_local[1]

				# horario_primer_turno = diccionario_horarios[primer_turno]
				horario_primer_turno = self._obtengo_horario_de_rubro(diccionario_horarios, primer_turno)

				if 'cetp' in segundo_turno:
					horarios_cetp = self.obtengo_diccionario_horarios('cetp')
					horario_segundo_turno = self._obtengo_horario_de_rubro(horarios_cetp, segundo_turno)

				elif 'ceip' in segundo_turno:
					horarios_ceip = self.obtengo_diccionario_horarios('ceip')
					horario_segundo_turno = self._obtengo_horario_de_rubro(horarios_ceip, segundo_turno)

				elif 'ces' in segundo_turno:
					horarios_ces = self.obtengo_diccionario_horarios('ces')
					horario_segundo_turno = self._obtengo_horario_de_rubro(horarios_ces, segundo_turno)

				else:
					# En caso de no ser ninguno de los anteriores es un turno que no comparte con alguna institucion
					# con rubro diferente a la que se esta buscando
					horario_segundo_turno = self._obtengo_horario_de_rubro(diccionario_horarios, segundo_turno)

				# Horario total corresponde al comienzo del primer horario (por la mañana) y el final del ultimo
				# horario (por la tarde), de esta manera se tiene un horario total en el que esta operativo el local.
				horario_total = (horario_primer_turno[0], horario_segundo_turno[1])

				return horario_total

			elif largo_de_lista_turnos == 1:
				# Si tiene definido un solo turno se asume siempre que el rubro es el global del local
				horario_primer_turno = self._obtengo_horario_de_rubro(diccionario_horarios, turnos_local[0])
				horario_total = (horario_primer_turno[0], horario_primer_turno[1])

				return horario_total

		else:
			# Retornar horario por defecto cuando no tiene rubro turno
			horario_total = (('- 7200', '07:00:00'), ('+ 25200', '17:00:00'))
			return horario_total

