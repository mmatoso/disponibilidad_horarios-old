"""
Modulo principal de ejecución, recopila información y utiliza los otros modulos para obtener los datos del local
educativo y así calcular su disponibilidad y guardarlo en base de datos.
"""

from datetime import datetime, timedelta

from datos_bdd_and_crm.cursores_bdd import consulta_disponibilidad
from zabbix.datos_zabbix import existe_dia
from datos_bdd_and_crm.datos_bdd_and_crm import lista_de_locales, existe_mes, generar_tupla_tabla_locales, \
    generar_tupla_tabla_disponibilidad


def main():

    # Se calcula el dia anterior en el que se corre el script ya que se ejecuta a mes vencido
    hoy = datetime.today()
    ayer = hoy - timedelta(days=1)
    ayer_string = ayer.strftime('%Y-%m-%d')
    anio = ayer.strftime('%Y')
    mes = ayer.strftime('%m')
    dia = ayer.strftime('%d')
    mes_string = f'{anio}-{mes}-01'

    dia = existe_dia(ayer_string)

    # Si la fecha de hoy SI se encuentra
    # que ese dia es un dia habil en la tabla DIAS de zabbix, por lo tanto se debe ejecutar el calculo.
    if dia:

        locales_produccion = lista_de_locales(mes_string)

        try:
            existe_el_mes = existe_mes(mes_string)

            if not existe_el_mes:

                print('inicia locales')
                # Inserto en base de datos en la tabla locales.
                for codicen in locales_produccion:

                    tupla_datos_escuela = generar_tupla_tabla_locales(codicen, mes_string)

                    query_escuela = """INSERT INTO locales (codicen, mes, acceso, crm, educativo, tipo_local,
                                                        alumnos, ad, altaPerformance, depto)
                                                        VALUES(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"""

                    with consulta_disponibilidad.cursor() as cursor:
                        cursor.execute(query_escuela, tupla_datos_escuela)

            # Inserto en base de datos para tabla disponibilidad.
            print('inicia disponibilidad')
            for codicen in locales_produccion:

                print(codicen)
                datos_local_tabla_disponibilidad = generar_tupla_tabla_disponibilidad(codicen, mes_string, ayer_string)
                query_disponibilidad = """
                INSERT INTO disponibilidad (dia, disponibilidad, codicen, mes, produccion)
                values (%s,%s,%s,%s,%s)"""

                with consulta_disponibilidad.cursor() as cursor:
                    cursor.execute(query_disponibilidad, datos_local_tabla_disponibilidad)

        except Exception as error:

            consulta_disponibilidad.rollback()
            print("rollback()")

            with open("log_errores.txt", "a") as file:
                file.write(f"{datetime.now()} - {error}" + "\n")
        else:
            print("commit()")
            consulta_disponibilidad.commit()
        finally:
            print('close()')
            consulta_disponibilidad.close()


if __name__ == '__main__':
    main()
