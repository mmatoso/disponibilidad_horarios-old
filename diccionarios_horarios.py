from datetime import time

mapeo_horario_por_defecto = {
	'defecto': (
		('- 7200', '+ 25200'),
		('07:00', '17:00'),
		(time(hour=7, minute=0, second=0), time(hour=17, minute=0, second=0))
	)
}

mapeo_horarios_ceip = {
	'matutino': (
		('- 3600', '+ 7200'),
		('08:00', '12:00'),
		(time(hour=8, minute=0, second=0), time(hour=12, minute=0, second=0))
	),
	'tiempo completo': (
		('- 3600', '+ 21600'),
		('08:00', '16:00'),
		(time(hour=8, minute=0, second=0), time(hour=16, minute=0, second=0))
	),
	'extendido': (
		('- 3600', '+ 25200'),
		('08:00', '17:00'),
		(time(hour=8, minute=0, second=0), time(hour=17, minute=0, second=0))
	),
	'especial': (
		('- 0', '+ 18000'),
		('09:00', '15:00'),
		(time(hour=9, minute=0, second=0), time(hour=15, minute=0, second=0))
	),
	'rural': (
		('+ 3600', '+ 18000'),
		('10:00', '15:00'),
		(time(hour=10, minute=0, second=0), time(hour=15, minute=0, second=0))
	),
	'vespertino': (
		('+ 14400', '+ 25200'),
		('13:00', '17:00'),
		(time(hour=13, minute=0, second=0), time(hour=17, minute=0, second=0))
	),
	'doble turno': (
		('- 3600', '+ 25200'),
		('08:00', '17:00'),
		(time(hour=8, minute=0, second=0), time(hour=17, minute=0, second=0))
	),

	'matutino ceip': (
		('- 3600', '+ 7200'),
		('08:00', '12:00'),
		(time(hour=8, minute=0, second=0), time(hour=12, minute=0, second=0))
	),
	'tiempo completo ceip': (
		('- 3600', '+ 21600'),
		('08:00', '16:00'),
		(time(hour=8, minute=0, second=0), time(hour=16, minute=0, second=0))
	),
	'extendido ceip': (
		('- 3600', '+ 25200'),
		('08:00', '17:00'),
		(time(hour=8, minute=0, second=0), time(hour=17, minute=0, second=0))
	),
	'especial ceip': (
		('- 0', '+ 18000'),
		('09:00', '15:00'),
		(time(hour=9, minute=0, second=0), time(hour=15, minute=0, second=0))
	),
	'rural ceip': (
		('+ 3600', '+ 18000'),
		('10:00', '15:00'),
		(time(hour=10, minute=0, second=0), time(hour=15, minute=0, second=0))
	),
	'vespertino ceip': (
		('+ 14400', '+ 25200'),
		('13:00', '17:00'),
		(time(hour=13, minute=0, second=0), time(hour=17, minute=0, second=0))
	),
	'doble turno ceip': (
		('- 3600', '+ 25200'),
		('08:00', '17:00'),
		(time(hour=8, minute=0, second=0), time(hour=17, minute=0, second=0))
	),
}

mapeo_horarios_ces = {
	'matutino': (
		('- 7200', '+ 14400'),
		('07:00', '14:00'),
		(time(hour=7, minute=0, second=0), time(hour=14, minute=0, second=0))
	),
	'matutino extendido': (
		('- 7200', '+ 14400'),
		('07:00', '14:00'),
		(time(hour=7, minute=0, second=0), time(hour=14, minute=0, second=0))
	),
	'multiturno': (
		('- 0', '+ 25200'),
		('09:00', '17:00'),
		(time(hour=9, minute=0, second=0), time(hour=17, minute=0, second=0))
	),
	'intermedio': (
		('+ 7200', '+ 28800'),
		('11:00', '18:00'),
		(time(hour=11, minute=0, second=0), time(hour=18, minute=0, second=0))
	),
	'intermedio extendido': (
		('+ 7200', '+ 28800'),
		('11:00', '18:00'),
		(time(hour=11, minute=0, second=0), time(hour=18, minute=0, second=0))
	),
	'vespertino': (
		('+ 10800', '+ 36000'),
		('12:30', '20:00'),
		(time(hour=12, minute=30, second=0), time(hour=20, minute=0, second=0))
	),
	'vespertino extendido': (
		('+ 10800', '+ 36000'),
		('12:30', '20:00'),
		(time(hour=13, minute=30, second=0), time(hour=20, minute=0, second=0))
	),
	'nocturno': (
		('+ 36000', '+ 50400'),
		('19:00', '24:00'),
		(time(hour=19, minute=0, second=0), time(hour=23, minute=59, second=0))
	),
	'nocturno extendido': (
		('+ 36000', '+ 50400'),
		('19:00', '24:00'),
		(time(hour=19, minute=0, second=0), time(hour=23, minute=59, second=0))
	),

	'matutino ces': (
		('- 7200', '+ 14400'),
		('07:00', '14:00'),
		(time(hour=7, minute=0, second=0), time(hour=14, minute=0, second=0))
	),
	'matutino extendido ces': (
		('- 7200', '+ 14400'),
		('07:00', '14:00'),
		(time(hour=7, minute=0, second=0), time(hour=14, minute=0, second=0))
	),
	'multiturno ces': (
		('- 0', '+ 25200'),
		('09:00', '17:00'),
		(time(hour=9, minute=0, second=0), time(hour=17, minute=0, second=0))
	),
	'intermedio ces': (
		('+ 7200', '+ 28800'),
		('11:00', '18:00'),
		(time(hour=11, minute=0, second=0), time(hour=18, minute=0, second=0))
	),
	'intermedio extendido ces': (
		('+ 7200', '+ 28800'),
		('11:00', '18:00'),
		(time(hour=11, minute=0, second=0), time(hour=18, minute=0, second=0))
	),
	'vespertino ces': (
		('+ 10800', '+ 36000'),
		('12:30', '20:00'),
		(time(hour=12, minute=30, second=0), time(hour=20, minute=0, second=0))
	),
	'vespertino extendido ces': (
		('+ 10800', '+ 36000'),
		('12:30', '20:00'),
		(time(hour=13, minute=30, second=0), time(hour=20, minute=0, second=0))
	),
	'nocturno ces': (
		('+ 36000', '+ 50400'),
		('19:00', '24:00'),
		(time(hour=19, minute=0, second=0), time(hour=23, minute=59, second=0))
	),
	'nocturno extendido ces': (
		('+ 36000', '+ 50400'),
		('19:00', '24:00'),
		(time(hour=19, minute=0, second=0), time(hour=23, minute=59, second=0))
	),
}

mapeo_horarios_cetp = {
	'matutino': (
		('- 7200', '+ 14400'),
		('07:00', '14:55'),
		(time(hour=7, minute=0, second=0), time(hour=14, minute=55, second=0))
	),
	'alternancia': (
		('- 7200', '+ 28800'),
		('07:00', '17:55'),
		(time(hour=7, minute=0, second=0), time(hour=17, minute=55, second=0))
	),
	'matutino vespertino': (
		('- 72000', '+ 25200'),
		('07:00', '16:55'),
		(time(hour=7, minute=0, second=0), time(hour=16, minute=55, second=0))
	),
	'vespertino': (
		('+ 14400', '+ 36000'),
		('13:00', '19:55'),
		(time(hour=13, minute=0, second=0), time(hour=19, minute=55, second=0))
	),
	'vespertino nocturno': (
		('+ 28800', '+ 46800'),
		('17:00', '22:55'),
		(time(hour=17, minute=0, second=0), time(hour=22, minute=55, second=0))
	),
	'nocturno': (
		('+ 32400', '+ 50400'),
		('18:00', '23:59'),
		(time(hour=18, minute=0, second=0), time(hour=23, minute=59, second=0))
	),
	'doble horario': (
		('- 7200', '+ 36000'),
		('07:00', '19:55'),
		(time(hour=18, minute=0, second=0), time(hour=23, minute=59, second=0))
	),
	'matutino cetp': (
		('- 7200', '+ 14400'),
		('07:00', '13:55'),
		(time(hour=7, minute=0, second=0), time(hour=13, minute=55, second=0))
	),
	'alternancia cetp': (
		('- 7200', '+ 28800'),
		('07:00', '17:45'),
		(time(hour=7, minute=0, second=0), time(hour=17, minute=45, second=0))
	),
	'matutino vespertino cetp': (
		('- 72000', '+ 28800'),
		('07:00', '17:55'),
		(time(hour=7, minute=0, second=0), time(hour=17, minute=55, second=0))
	),
	'vespertino cetp': (
		('+ 14400', '+ 36000'),
		('13:00', '19:55'),
		(time(hour=13, minute=0, second=0), time(hour=19, minute=55, second=0))
	),
	'vespertino nocturno cetp': (
		('+ 28800', '+ 46800'),
		('17:00', '22:55'),
		(time(hour=17, minute=0, second=0), time(hour=22, minute=55, second=0))
	),
	'nocturno cetp': (
		('+ 32400', '+ 50400'),
		('18:00', '23:59'),
		(time(hour=18, minute=0, second=0), time(hour=23, minute=59, second=0))
	),
	'doble horario cetp': (
		('- 7200', '+ 36000'),
		('07:00', '19:55'),
		(time(hour=18, minute=0, second=0), time(hour=23, minute=59, second=0))
	),
}