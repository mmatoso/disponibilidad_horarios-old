"""
Obtiene datos procedentes de la base de datos de zabbix
"""
import math
from datetime import datetime, timedelta

from datos_bdd_and_crm.cursores_bdd import consulta_zabbix
from horarios.horario import Horarios


def grupos_asignados_codicen(codicen):
    """
    Obtiene los grupos asignados al codicen
    :return: lista con todos los grupos del local asociados tanto a router como servidor (si tienen estos equipos)
    """

    consulta_hosts = f"""select hosts.host as host from hosts, hosts_templates
           where substring_index(hosts.host,'_',1) = '{codicen}'
           and hosts.hostid = hosts_templates.hostid
           and (hosts_templates.templateid = '23313' or hosts_templates.templateid = '10084'
           or hosts_templates.templateid = '23008' or hosts_templates.templateid = '34938'
           or hosts_templates.templateid = '10085');"""

    with consulta_zabbix.cursor() as cursor:
        cursor.execute(consulta_hosts)
        hosts = cursor.fetchall()

    hosts = [host.get('host') for host in hosts]
    # numero_hosts = len(hosts)

    try:
        grupo_1 = hosts[0]
    except IndexError:
        grupo_1 = None

    try:
        grupo_2 = hosts[1]
    except IndexError:
        grupo_2 = None

    query_grupos = f"""select distinct `groups`.name as grupo from hosts, hosts_groups, `groups`
           where hosts.hostid = hosts_groups.hostid
           and hosts_groups.groupid = `groups`.groupid
           and (hosts.host = '{grupo_1}' or hosts.host = '{grupo_2}');"""

    with consulta_zabbix.cursor() as cursor:
        cursor.execute(query_grupos)
        grupos_local = cursor.fetchall()

    grupos_local = [grupo.get('grupo') for grupo in grupos_local] if grupos_local else None

    return grupos_local


def valores_minimos_disponibilidad(horas):
    """
    Obtiene a partir de las franjas horarias cuantos son los valores minimos aceptables para considerar valida la
    disponibilidad. En caso de no tener los valores minimos el local se considera como null.
    :param horas: tupla de horario obtenida del modulo horario.py el cual calcula la franja horaria de un local.
    :return: valores minimos de disponibilidad
    """
    hora_inicio = horas[0][1]
    hora_final = horas[1][1]

    hora_inicio_datetime = datetime.strptime(hora_inicio, '%H:%M:%S')
    hora_final_datetime = datetime.strptime(hora_final, '%H:%M:%S')

    diferencia_horaria = hora_final_datetime - timedelta(
        hours=hora_inicio_datetime.hour,
        minutes=hora_inicio_datetime.minute
    )

    # Redondeo hacia abajo en caso de decimales
    valores_minimos = math.ceil(int(diferencia_horaria.strftime('%H')) / 2)

    return valores_minimos


def obtener_disponibilidad(codicen, dia_string):

    """
    Calcula la disponibilidad promedio de un local para un dia determinado
    :param codicen: el codicen para calcular la disponibilidad
    :param dia_string: el dia a calcular la disponibilidad como string '2020-10-24'
    :return: disponibilidad o null en caso de no haber datos
    """

    try:
        horario = Horarios(codicen)
    except:
        horario = (('- 7200', '07:00:00'), ('+ 25200', '17:00:00'))
    else:
        horario = horario.obtengo_horarios()

    horario_inicial = horario[0][0]
    horario_final = horario[1][0]

    valores_minimos = valores_minimos_disponibilidad(horario)

    # El comodin % se pone doble porque hay un bug con mysqldb, lo interpreta como formateo de string python.
    consulta = f"""select avg(trends.value_avg) as disponibilidad, count(trends.value_avg) as valores_disponibilidad
    from hosts,items,trends,`groups`,hosts_groups,dias
    where hosts.hostid = hosts_groups.hostid and `groups`.groupid = hosts_groups.groupid
    and `groups`.name like 'Zona%%Infra' and items.name = 'Disponibilidad WLAN'
    and hosts.host like '{codicen}_%%' and items.hostid = hosts.hostid and items.itemid = trends.itemid
    and date(from_unixtime(trends.clock)) = '{dia_string}' and trends.clock >= (dias.clock {horario_inicial})
    and trends.clock <= (dias.clock {horario_final});"""

    with consulta_zabbix.cursor() as cursor:
        cursor.execute(consulta)
        resultado = cursor.fetchall()

    valores_disponibilidad = resultado[0].get('valores_disponibilidad')  # Si no hay valores viene 0

    if valores_disponibilidad >= valores_minimos:
        disponibilidad = resultado[0].get('disponibilidad')
    else:
        disponibilidad = None

    return disponibilidad


def obtener_locales_grupos_infra():
    """
    Obtiene un listado de todos los locales que se encuentran en grupos INFRA en zabbix
    :return: lista con locales en grupos INFRA
    """

    # Consulta que obtiene todos los hosts en grupos %_INFRA, locales sin este grupo no aparecen,
    # Ejemplo: solo piloto, sitios en altas.

    query_locales_infra_zabbix = """select distinct(SUBSTRING_INDEX(hosts.host,'_',1)) as host
                      from hosts, `groups`,hosts_groups
                      where hosts.hostid = hosts_groups.hostid
                      and hosts_groups.groupid = `groups`.groupid
                      and `groups`.name like '%%_INFRA'"""

    with consulta_zabbix.cursor() as cursor:
        cursor.execute(query_locales_infra_zabbix)
        locales_infra = cursor.fetchall()

    locales_infra = set([item.get('host') for item in locales_infra])  # Elimino duplicados con set()
    locales_infra = list(locales_infra)  # Va a retornar esta lista en caso de que sea primero de mes

    return locales_infra


def existe_dia(dia_string):
    """
    A partir de una fecha, obtiene si la misma existe en la tabla dias de zabbix
    utilizado para decidir si se obtienen datos de disponibilidad o no para ese dia especifico.
    :param dia_string: un string que representa una fecha '2020-01-01'
    :return: True or False
    """

    existe_dia = f"SELECT DATE(from_unixtime(clock)) as dia FROM dias WHERE DATE(from_unixtime(clock)) = '{dia_string}';"

    with consulta_zabbix.cursor() as cursor:
        cursor.execute(existe_dia)
        lista_fechas = cursor.fetchall()

    lista_fechas = [str(dia.get('dia')) for dia in lista_fechas]

    if dia_string in lista_fechas:
        return True
    else:
        return False


def es_produccion(codicen):
    """
    La funcion detecta si el codicen pertenece a un grupo produccion o no a partir de sus grupos asignados en zabbix.
    :return: True or False en base a si es produccion o no
    """

    grupos_no_roduccion = [
        'Sitios_con_conectividad_parcial',
        'PILOTOS',
        'Sitios_Cerrados',
        'Sitios_no_Monitoreados',
        'Sitios_Sin_Soporte_Activo',
        'Sitios_en_Alta'
    ]

    grupos_local = grupos_asignados_codicen(codicen)  # Obtengo los grupos asignados

    grupos = []
    if grupos_local:
        for grupo in grupos_local:
            grupos.append(grupo)

    # Busco algun grupo no produccion dentro de grupos
    contiene_no_produccion = [item for item in grupos_no_roduccion if item in grupos]
    # Busco grupos PILOTO dentro de los grupos
    contiene_pilotos = [item for item in grupos if "PILOTOS" in grupos]

    # Si el grupo no produccion es PILOTO y es el unico grupo que contiene se considera NO PRODUCCION.
    # Si el local tiene el grupo PILOTOS pero NO es el unico grupo se considera produccion.
    if contiene_no_produccion and contiene_pilotos:

        # La lista  contiene_no_produccion tiene el grupo pilotos y ademas contiene_pilotos contiene PILOTOS
        # y es su unico valor, entonces se considera NO PRODUCCION
        if "PILOTOS" in contiene_no_produccion and (len(contiene_pilotos) == 1 and "PILOTOS" in contiene_pilotos):
            es_produccion = False
        else:
            es_produccion = True

    elif contiene_no_produccion:
        # Si la lista contiene datos significa que el local esta en al menos un grupo no produccion
        es_produccion = False

    else:
        # si no se cumple ninguna de las condiciones es produccion.
        es_produccion = True

    return es_produccion


