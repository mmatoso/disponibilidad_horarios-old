import pymysql.cursors
import pandas as pd
import datetime
from ip_variables import *
from datetime import datetime, timedelta


consulta_churrinche = pymysql.connect(
    host=HOST_CHURRINCHE,
    user=USER_DISPONIBILIDAD,
    password=PASS_DISPONIBILIDAD,
    db=DB_NAME_DISPONIBILIDAD,
    cursorclass=pymysql.cursors.DictCursor
)

consulta_zabbix = pymysql.connect(
    host=HOST_ZABBIX,
    user=USER_ZABBIX,
    password=PASS_ZABBIX,
    db=DB_NAME_ZABBIX,
    cursorclass=pymysql.cursors.DictCursor
)


def obtener_alumnos(codicen, fecha_inicio, fecha_final):
    """
    Obtiene los alumnos de la base de datos de disponibilidad_horarios, se asume que el codicen siempre existe
    o en el mes inicio o en el mes final (en caso de que los periodos esten distribuidos en dos meses), pues el codicen
    sale de la base de datos de disponibilidad.
    :param codicen: el codicen correspondiente
    :param fecha_inicio: fecha de inicio de busqueda para el periodo
    :param fecha_final: fecha final de busqueda para el periodo
    :return: cantidad de alumnos
    """

    mes_ini = fecha_inicio.split("-")
    mes_ini = '{}-{}-01'.format(mes_ini[0], mes_ini[1])

    mes_fin = fecha_final.split("-")
    mes_fin = '{}-{}-01'.format(mes_fin[0], mes_fin[1])

    query_alumnos = "SELECT alumnos FROM `locales` WHERE codicen = '{}' AND mes = '{}'".format(codicen, mes_fin)

    with consulta_churrinche.cursor() as cursor:
        cursor.execute(query_alumnos)
        alumnos = cursor.fetchall()

    alumnos = [alumno.get('alumnos') for alumno in alumnos] if alumnos else [None]
    alumnos = alumnos[0]

    if alumnos is None:  # Si no hay ningun alumno en el mes fin, busco en el mes inicio
        query_alumnos = "SELECT alumnos FROM `locales` WHERE codicen = '{}' AND mes = '{}'".format(codicen, mes_ini)

        with consulta_churrinche.cursor() as cursor:
            cursor.execute(query_alumnos)
            alumnos = cursor.fetchall()

        alumnos = [alumno.get('alumnos') for alumno in alumnos]
        alumnos = alumnos[0]

    return alumnos


def obtener_disponibilidad(codicen, fecha_inicio, fecha_final):
    """
    Obtiene la disponibilidad para un periodo determinado y un codicen dado
    :param codicen: el codicen correspondiente al local
    :param fecha_inicio: fecha de inicio para el rango
    :param fecha_final: fecha final para el rango
    :return: promedio de disponibilidad para el codicen dado entre las fechas dadas
    """

    query_disponibilidad = """SELECT format(AVG(disponibilidad)*100,2) AS disponibilidad
            FROM `disponibilidad` AS d INNER JOIN locales AS e ON d.codicen = e.codicen 
            AND d.mes = e.mes
            WHERE e.educativo = 1 
            AND d.produccion = 1
            AND dia >= "{}" AND dia <= "{}"
            AND d.codicen = "{}" GROUP BY d.codicen""".format(fecha_inicio, fecha_final, codicen)

    with consulta_churrinche.cursor() as cursor:
        cursor.execute(query_disponibilidad)
        disponibilidad = cursor.fetchall()

    disponibilidad = [valor.get('disponibilidad') for valor in disponibilidad] if disponibilidad else [None]
    disponibilidad = disponibilidad[0]

    return disponibilidad


def crear_lista_disponibilidad(lista, fecha_inicio, fecha_final):
    """
    Crea una lista de diccionarios que contiene toda la informacion necesaria para calcular la disponibilidad mef
    dicha lista contiene, codicen, alumnos, disponibilidad y el dia en que se ejecuta.
    :param lista: lista de codicen
    :param fecha_inicio: fecha de inicio que responde a la ventana de 20 dias habiles MEF
    :param fecha_final: fecha final que responde a la ventana de 20 dias habiles MEF
    :return: lista con todos los codicen y su disponibilidad
    """

    lista_dict = []

    for codicen in lista:
        alumnos = obtener_alumnos(codicen, fecha_inicio, fecha_final)
        disponibilidad = obtener_disponibilidad(codicen, fecha_inicio, fecha_final)

        diccionario_local = {
            'dia': fecha_final,
            'codicen': codicen,
            'disponibilidad': disponibilidad,
            'alumnos': alumnos
        }

        lista_dict.append(diccionario_local)

    return lista_dict


def obtener_ultimos_veinte_dias_habiles(dia):
    """
    A partir de un dia determinado obtiene los últimos 20 días hábiles de la tabla dias de zabbix
    :param dia: un dia en formato 'año-mes-dia'
    :return:
    """

    query_fechas = """select date(from_unixtime(clock)) as dia from dias where
    date(from_unixtime(clock)) <= '{}' order by clock DESC limit 20;""".format(dia)

    with consulta_zabbix.cursor() as cursor:
        cursor.execute(query_fechas)
        fechas = cursor.fetchall()

    lista_fechas = [str(fecha.get('dia')) for fecha in fechas]

    return lista_fechas


def obtener_locales_educativos_produccion(fecha_inicio, fecha_final):
    """
    A partir de dos fechas determinadas obtiene una lista de locales comprendidos en ese rango, la lista se obtiene
    de la base de datos de disponibilidad_horarios, los locales son educativos y produccion
    :param fecha_inicio: la fecha de inicio de busqueda
    :param fecha_final: la fecha final de busqueda
    :return: lista con locales
    """

    consulta_locales = """SELECT DISTINCT d.codicen FROM `disponibilidad` AS d INNER JOIN locales AS e
    ON d.codicen = e.codicen AND d.mes = e.mes WHERE e.educativo = 1
    AND d.produccion = 1 AND dia >= '{}' AND dia <= '{}'""".format(fecha_inicio, fecha_final)

    with consulta_churrinche.cursor() as cursor:
        cursor.execute(consulta_locales)
        locales = cursor.fetchall()

    lista = [codicen.get('codicen') for codicen in locales]

    return lista


def calculo_franjas(lista_locales):

    """
    Crea un dataFrame a partir de la lista obtenida de la funcion crear_lista_disponibilidad()
    :param lista_locales: la lista obtenida de crear_lista_disponibilidad()
    :return:
    """

    # creo data frame
    df = pd.DataFrame(lista_locales)
    df = df[df['disponibilidad'].notnull()]  # Elimino Nones del calculo de disponibilidad

    # Total de alumnos para poder ponderar en cada franja
    total_alumnos = df['alumnos'].astype(int).sum()

    # Promedio disponibilidad por dia
    promedio_total = df['disponibilidad'].astype(float).sum()/df['disponibilidad'].count()

    # Ponderado por alumnos
    df_mas_noventa_cinco = df[df['disponibilidad'].astype(float) >= 95]
    alumnos_mas_noventa_cinco = df_mas_noventa_cinco['alumnos'].astype(int).sum()
    df_mas_noventa_cinco = df_mas_noventa_cinco['alumnos'].astype(float).sum()/total_alumnos*100

    df_mas_setenta_cinco = df[(df['disponibilidad'].astype(float) >= 75) & (df['disponibilidad'].astype(float) < 95)]
    alumnos_mas_setenta_cinco = df_mas_setenta_cinco['alumnos'].astype(int).sum()
    df_mas_setenta_cinco = df_mas_setenta_cinco['alumnos'].astype(float).sum()/total_alumnos*100

    df_mas_cincuenta = df[(df['disponibilidad'].astype(float) >= 50) & (df['disponibilidad'].astype(float) < 75)]
    alumnos_mas_cincuenta = df_mas_cincuenta['alumnos'].astype(int).sum()
    df_mas_cincuenta = df_mas_cincuenta['alumnos'].astype(float).sum()/total_alumnos*100

    df_menor_cincuenta = df[(df['disponibilidad'].astype(float) > 0) & (df['disponibilidad'].astype(float) < 50)]
    alumnos_menor_cincuenta = df_menor_cincuenta['alumnos'].astype(int).sum()
    df_menor_cincuenta = df_menor_cincuenta['alumnos'].astype(float).sum()/total_alumnos*100

    df_cero = df[df['disponibilidad'].astype(float) == 0]
    alumnos_cero = df_cero['alumnos'].astype(int).sum()
    df_cero = df_cero['alumnos'].astype(float).sum()/total_alumnos*100

    locales_95 = df[df['disponibilidad'].astype(float) >= 95]['codicen'].count()
    locales_75 = df[(df['disponibilidad'].astype(float) >= 75) & (df['disponibilidad'].astype(float) < 95)]['codicen'].count()
    locales_50 = df[(df['disponibilidad'].astype(float) >= 50) & (df['disponibilidad'].astype(float) < 75)]['codicen'].count()
    locales_1 = df[(df['disponibilidad'].astype(float) > 0) & (df['disponibilidad'].astype(float) < 50)]['codicen'].count()
    locales_0 = df[df['disponibilidad'].astype(float) == 0]['codicen'].count()

    # franja del 95 porciento
    lista_95 = [round(df_mas_noventa_cinco, 2), alumnos_mas_noventa_cinco, locales_95]
    lista_75 = [round(df_mas_setenta_cinco, 2), alumnos_mas_setenta_cinco, locales_75]
    lista_50 = [round(df_mas_cincuenta, 2), alumnos_mas_cincuenta, locales_50]
    lista_1 = [round(df_menor_cincuenta, 2), alumnos_menor_cincuenta, locales_1]
    lista_0 = [round(df_cero, 2), alumnos_cero, locales_0]
    lista_total = [round(promedio_total, 1)]

    diccionario_franjas = {
        'lista_95': lista_95,
        'lista_75': lista_75,
        'lista_50': lista_50,
        'lista_1': lista_1,
        'lista_0': lista_0,
        'lista_total': lista_total
    }

    return diccionario_franjas


def main():

    hoy = datetime.today()
    ayer = hoy - timedelta(days=1)
    ayer = ayer.strftime('%Y-%m-%d')

    fechas = obtener_ultimos_veinte_dias_habiles(ayer)
    fecha_ini = fechas[-1]
    fecha_fin = fechas[0]

    lista_locales = obtener_locales_educativos_produccion(fecha_ini, fecha_fin)

    datos_locales = crear_lista_disponibilidad(lista_locales, fecha_ini, fecha_fin)

    franjas = calculo_franjas(datos_locales)
    d_95 = franjas['lista_95']
    d_75 = franjas['lista_75']
    d_50 = franjas['lista_50']
    d_1 = franjas['lista_1']
    d_0 = franjas['lista_0']

    insert_95 = """INSERT INTO MEF (dia, umbral, `%`,alumnos, locales)
    VALUES('{}','mayor_igual_95', {}, {}, {})""".format(ayer, d_95[0], d_95[1], d_95[2])

    insert_75 = """INSERT INTO MEF (dia, umbral, `%`,alumnos, locales)
    VALUES('{}','mayor_igual_75', {}, {}, {})""".format(ayer, d_75[0], d_75[1], d_75[2])

    insert_50 = """INSERT INTO MEF (dia, umbral, `%`,alumnos, locales)
    VALUES('{}','mayor_igual_50', {}, {}, {})""".format(ayer, d_50[0], d_50[1], d_50[2])

    insert_1 = """INSERT INTO MEF (dia, umbral, `%`,alumnos, locales)
    VALUES('{}','mayor_igual_1', {}, {}, {})""".format(ayer, d_1[0], d_1[1], d_1[2])

    insert_0 = """INSERT INTO MEF (dia, umbral, `%`,alumnos, locales)
    VALUES('{}','igual_0', {}, {}, {})""".format(ayer, d_0[0], d_0[1], d_0[2])


    try:
        print("grabo en base")
        with consulta_churrinche.cursor() as cursor:
            cursor.execute(insert_95)
            cursor.execute(insert_75)
            cursor.execute(insert_50)
            cursor.execute(insert_1)
            cursor.execute(insert_0)
    except Exception as error:
        print("error", error.message)
        print("error rollback")
        consulta_churrinche.rollback()
    else:
        print("commit")
        consulta_churrinche.commit()
    finally:
        print("cierro conexion")
        cursor.close()
        consulta_churrinche.close()
    print(insert_95, insert_75, insert_50, insert_1, insert_0)


if __name__ == '__main__':
    main()

